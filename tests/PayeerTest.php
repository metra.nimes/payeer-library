<?php

require('vendor/autoload.php');

use Library\Core\Driver\Payeer;
use Library\Core\HttpClient\GuzzleHttpClient;
use PHPUnit\Framework\TestCase;

class PayeerTest extends TestCase {

	public function testOrders() {
		$driver = new Payeer();
		$driver->setOptions(
			[
				'url' => 'https://payeer.com/api/trade',
				'credentials' => [
					'id' => 'e1520c3c-9804-4efd-9180-63db99fa489e'
				],
			]
		);
		$driver->setHttpClient(new GuzzleHttpClient([]));

		$data = $driver->orders('BTC_RUB');
		$this->assertTrue($data['success'] ?? FALSE);
	}
}