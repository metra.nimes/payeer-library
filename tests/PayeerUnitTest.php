<?php

require('vendor/autoload.php');

use Library\Core\Driver\Payeer;
use PHPUnit\Framework\TestCase;


class PayeerUnitTest extends TestCase {

	/**
	 * @dataProvider getValues
	 * @param string $options
	 * @param array $expectedMessage
	 */
	public function testSetOptionsExceptions(array $options, string $expectedMessage) {
		$driver = new Payeer();
		$this->expectException(\Exception::class);
		$this->expectExceptionMessage($expectedMessage);
		$driver->setOptions($options);
	}

	public function getValues()
	{
		return [
			[
				[
					'url' => '',
				],
				'Empty Url.'
			],
			[
				[
					'url' => 'trade',
				],
				'Invalid Url.'
			],
			[
				[
					'url' => 'https://payeer.com/api/trade',
				],
				'Invalid credentials id.'
			],
			[
				[
					'url' => 'https://payeer.com/api/trade',
					'credentials' => [
						'id' => '34634745745784584658454',
					],
				],
				'Invalid credentials secret key.'
			],
		];
	}

}