<?php


require_once "vendor/autoload.php";

use \Library\Core\Driver\Payeer;
use \Library\Core\HttpClient\GuzzleHttpClient;

$driver = new Payeer();
$driver->setOptions(
	[
		'url' => 'https://payeer.com/api/trade',
		'credentials' => [
			'id' => 'e1520c3c-9804-4efd-9180-63db99fa489e',
			'secretKey' => '****',
		],
	]
);
$driver->setHttpClient(new GuzzleHttpClient([]));
var_dump($driver->info());