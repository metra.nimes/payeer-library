<?php

namespace Library\Core\Driver;

use Library\Core\DriverInterface;
use Library\Core\Enum\DriverError;
use Library\Core\HttpClient\HttpClientInterface;
use Exception;

/**
 * @method string message(DriverError $error)
 */
class Payeer implements DriverInterface
{
    /** @var array<mixed> $options */
    protected $options = [];

    /** @var HttpClientInterface $client */
    protected $client;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->options['url'] ?? '';
    }

    /**
     * @param array<mixed> $options
     * @return void
     * @throws \Exception
     */
    public function setOptions(array $options): void
    {
        $options = array_filter($options);
        $this->options = $options;

        $this->validateUrl();
        $this->validateCredentials();
    }

    /**
     * @return array<mixed>
     */
    public function getCredentials(): array
    {
        return $this->options['credentials'];
    }

    /**
     * @param string $method
     * @return string
     */
    protected function generateSign(string $method = 'account'): string
    {
        try {
            ['secretKey' => $secretKey] = $this->getCredentials();
            $req = json_encode(value: ['ts' => $this->getMsec()]);
            $sign = hash_hmac('sha256', $method . $req, $secretKey);
        } catch (Exception) {
            return '';
        }

        return $sign;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function validateUrl(): void
    {
        if (empty($this->getUrl())) {
            throw new Exception($this->message(DriverError::E_EMPTY_URL));
        }

        if (filter_var($this->getUrl(), FILTER_VALIDATE_URL) === false) {
            throw new Exception($this->message(DriverError::E_INVALID_URL));
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function validateCredentials(): void
    {
        $credentials = $this->getCredentials();

        if (empty($credentials['id'])) {
            throw new Exception($this->message(DriverError::E_INVALID_CREDENTIALS_ID));
        }

        if (empty($credentials['secretKey'])) {
            throw new Exception($this->message(DriverError::E_INVALID_CREDENTIALS_SECRET_KEY));
        }
    }

    /**
     * @return array<mixed>
     * @throws Exception
     */
    public function info(): array
    {
        return $this->getResponse('info');
    }

    /**
     * @param string $pair
     * @return array<mixed>
     * @throws Exception
     */
    public function orders(string $pair = 'BTC_USDT'): array
    {
        return $this->getResponse('orders', [
            'pair' => $pair,
        ]);
    }

    /**
     * @return array<mixed>
     * @throws Exception
     */
    public function account(): array
    {
        return $this->getResponse('account', [
            'ts' => $this->getMsec(),
        ]);
    }

    /**
     * @param array<mixed> $req
     * @return array<mixed>
     * @throws Exception
     */
    public function orderCreate(array $req): array
    {
        return $this->getResponse('order_create', $req);
    }

    /**
     * @param string $orderId
     * @return array<mixed>
     * @throws Exception
     */
    public function orderStatus(string $orderId): array
    {
        return $this->getResponse('order_status', [
            'ts' => $this->getMsec(),
            'order_id' => $orderId
        ]);
    }

    /**
     * @return array<mixed>
     * @throws Exception
     */
    public function myOrders(): array
    {
        return $this->getResponse('my_orders', ['ts' => $this->getMsec()]);
    }

    /**
     * @param HttpClientInterface $client
     * @return void
     */
    public function setHttpClient(HttpClientInterface $client): void
    {
        $this->client = $client;
    }

    /**
     * @return array<mixed>
     */
    protected function getHeaders(string $uri): array
    {
        ['id' => $id] = $this->getCredentials();

        $sign = $this->generateSign($uri);

        return [
            'Content-Type' => 'application/json',
            'API-ID' => $id,
            'API-SIGN' => $sign,
        ];
    }

    /**
     * @return float
     */
    protected function getMsec(): float
    {
        return round(microtime(true) * 1000);
    }

    /**
     * @param string $name
     * @param array<mixed> $args
     * @return string
     * @throws Exception
     */
    public function __call(string $name, array $args)
    {
        return match ($name) {
            'message' => DriverError::message($args[0]),
            default => throw new Exception('Unknown method.'),
        };
    }

    /**
     * @param string $uri
     * @param array<mixed> $body
     * @return array<mixed>
     * @throws Exception
     */
    protected function getResponse(string $uri, array $body = []): array
    {
        try {
            /** @var HttpClientInterface $response */
            $response = $this->client
                ->request('POST', $this->getUrl() . '/' . $uri, [
                    'headers' => $this->getHeaders($uri),
                    'body' => json_encode($body)
                ]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $response->getBody();
    }
}
