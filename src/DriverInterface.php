<?php

namespace Library\Core;

use Library\Core\HttpClient\HttpClientInterface;

interface DriverInterface
{
    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return array<mixed>
     */
    public function getCredentials(): array;

    /**
     * @return void
     */
    public function validateCredentials(): void;

    /**
     * @return void
     */
    public function validateUrl(): void;

    /**
     * @param array<mixed> $options
     * @return void
     */
    public function setOptions(array $options): void;

    /**
     * @param HttpClientInterface $client
     * @return void
     */
    public function setHttpClient(HttpClientInterface $client): void;
}
