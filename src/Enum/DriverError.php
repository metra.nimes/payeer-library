<?php

namespace Library\Core\Enum;

enum DriverError
{
    case E_INVALID_CREDENTIALS_ID;
    case E_INVALID_CREDENTIALS_SECRET_KEY;
    case E_INVALID_URL;
    case E_EMPTY_URL;
    case E_INVALID_REQUIRED_OPTIONS;


    public static function message(self $value): string
    {
        return match ($value) {
            self::E_INVALID_CREDENTIALS_ID => 'Invalid credentials id.',
            self::E_INVALID_CREDENTIALS_SECRET_KEY => 'Invalid credentials secret key.',
            self::E_INVALID_URL => 'Invalid Url.',
            self::E_INVALID_REQUIRED_OPTIONS => 'Required options is empty.',
            self::E_EMPTY_URL => 'Empty Url.',
        };
    }
}
