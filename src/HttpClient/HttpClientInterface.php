<?php

namespace Library\Core\HttpClient;

interface HttpClientInterface
{
    /**
     * @param array<mixed> $params
     */
    public function __construct(array $params);

    /**
     * @return HttpClientInterface
     */
    public function getClient(): HttpClientInterface;

    /**
     * @param string $method
     * @param string $uri
     * @param  array<mixed> $params
     * @return HttpClientInterface
     */
    public function request(string $method, string $uri, array $params): HttpClientInterface;

    /**
     * @return array<mixed>
     */
    public function getBody(): array;

    /**
     * @return int
     */
    public function getStatus(): int;
}
