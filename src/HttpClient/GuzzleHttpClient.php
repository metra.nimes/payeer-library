<?php

namespace Library\Core\HttpClient;

use GuzzleHttp\Client;
use Exception;

class GuzzleHttpClient implements HttpClientInterface
{
    /** @var Client $client */
    protected $client;
    /** @var mixed $response */
    protected $response;

    /**
     * @param array<mixed> $params
     */
    public function __construct(array $params)
    {
        $this->client = new Client($params);
    }

    /**
     * @return HttpClientInterface
     */
    public function getClient(): HttpClientInterface
    {
        return $this;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param mixed[] $params
     * @return HttpClientInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $method, string $uri, array $params = []): HttpClientInterface
    {
        $this->response = $this->client->request($method, $uri, $params);

        return $this;
    }

    /**
     * @return array<mixed>
     * @throws Exception
     */
    public function getBody(): array
    {
        $body = ($this->response !== null) ? $this->response->getBody()->getContents() : '{}';

        try {
            $body = json_decode($body, true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $body;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->response->getStatusCode();
    }
}
