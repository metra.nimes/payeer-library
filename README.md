## Payeer library

Start Docker:

```
cp .env.example .env
docker-compose up -d 
```

Run Example:

```
docker-compose exec php php example.php
```

Run Test:

```
docker-compose exec php vendor/bin/phpunit tests/PayeerTest.php
```